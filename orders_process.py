from datetime import date 

class Process:
    orders = []

    @classmethod
    def start(cls):
        file = open('./orders.txt', 'r')
        for line in file:
            order_dict = eval(line)
            order = Order(order_dict['orderId'], order_dict['date'], order_dict['amount'])
            cls.orders.append(order)

        # Processing today transactions    
        Order.process_transactions(cls.orders, date(2019, 10, 18))


class Order:
    def __init__(self, orderId, date, amount):
        self.orderId = orderId
        self.date = date 
        self.amount = amount 

    @staticmethod
    def process_transactions(orders, date=date.today()):
        rephrase_today_date = "{day}-{month}-{year}".format(day=date.day, month=date.month, year=date.year)

        total = 0
        for order in orders:
            if(order.date == rephrase_today_date):
                total = total + order.amount

        print("{total} for today".format(total=total))

Process.start()